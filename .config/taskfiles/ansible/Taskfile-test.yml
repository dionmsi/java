---
version: '3'

tasks:
  local:
    desc: Run the Ansible play on the local machine (or via WSL - see task summary)
    summary: |
      # Run the Ansible play on the local machine

      This task will use the inventory stored in `test/<OS>/inventory`, the playbook
      file stored in `test/<OS>/test.yml`, and the Ansible configuration file stored in
      `test/<OS>/ansible.cfg` to run the play. At the beginning of the play, you will
      be prompted for the sudo password.
    cmds:
      - task: local:test

  local:test:
    deps:
      - :symlink:{{.REPOSITORY_SUBTYPE}}
      - :install:python:requirements
    cmds:
      - .config/log info "Testing Ansible playbook locally"
      - cp test/{{OS}}/inventory inventory
      - if [ ! -f ansible.cfg.bak ]; then cp ansible.cfg ansible.cfg.bak; fi
      - cp test/{{OS}}/ansible.cfg ansible.cfg
      - |
        .config/log info 'Prompting for sudo password (required)'
        {{.PYTHON_HANDLE}}ansible-playbook --ask-sudo-pass test/{{OS}}/test.yml
      - mv ansible.cfg.bak ansible.cfg
      - rm inventory

  molecule:dependencies:
    cmds:
      - ansible-galaxy install --ignore-errors -r requirements.yml
      - task: :symlink:{{.REPOSITORY_SUBTYPE}}

  molecule:docker:
    deps:
      - :install:python:requirements
      - :install:software:docker
      - :install:software:sshpass
    label: test:docker
    desc: Performs a full test, including a test for idempotency, on all available Linux systems using Docker
    summary: |
      # Perform a full test, including an idempotency test, on all available Linux systems using Docker

      This task runs the project's Molecule tests using Docker. It only tests against
      Linux systems. If the role/project contains any steps that use the
      `community.general.snap` plugin, then only the operating systems that support that
      plugin with Docker are tested.

      **Example usage:**
      `task ansible:test:molecule:docker`

      **Save test results for use with auto-generating compatibility chart:**
      `task ansible:test:molecule:docker:matrix`
    cmds:
      - |
        SCENARIO="Linux"
        if grep -Ril 'community.general.snap:' ./tasks; then
          SCENARIO="Snap"
          .config/log warn 'Running Docker Molecule tests on the Docker containers that are compatible with `snap` since the role has references to `snap`'
        else
          .config/log info 'Running full Docker Molecule test'
        fi
        MOLECULE_GROUP="$SCENARIO" {{.PYTHON_HANDLE}}molecule test -s docker
        if [ $? != '0' ]; then
          .config/log error 'Molecule test finished with errors'
        else
          .config/log success 'Successfully ran Docker Molecule test'
        fi

  molecule:docker:cli:
    deps:
      - :install:python:requirements
      - :install:software:docker
      - :install:software:sshpass
    cmds:
      - |
        .config/log info 'Running Docker Molecule test on containers in the `{{.CLI_ARGS}}` group'
        MOLECULE_GROUP="{{.CLI_ARGS}}" {{.PYTHON_HANDLE}}molecule test -s docker
        if [ $? != '0' ]; then
          .config/log error 'The `{{.CLI_ARGS}}` Docker Molecule test finished with errors'
        else
          .config/log success 'Successfully ran the `{{.CLI_ARGS}}` Docker Molecule test'
        fi

  molecule:docker:matrix:
    deps:
      - :install:python:requirements
      - :install:software:docker
      - :install:software:sshpass
    vars:
      MOLECULE_DATE:
        sh: date '+%Y-%m-%d'
    cmds:
      - mkdir -p molecule/.results
      - |
        SCENARIO="Linux"
        if grep -Ril 'community.general.snap:' ./tasks; then
          SCENARIO="Snap"
          .config/log warn 'Running Docker Molecule tests on the Docker containers that are compatible with `snap` since the role has references to `snap`'
        else
          .config/log info 'Running full Docker Molecule test'
        fi
        .config/log info 'Piping results to `molecule/.results/{{.MOLECULE_DATE}}-$SCENARIO.txt` for compatibility chart data'
        PY_COLORS=0 MOLECULE_GROUP="$SCENARIO" {{.PYTHON_HANDLE}}molecule test -s "$SCENARIO" 2>&1 | tee "molecule/.results/{{.MOLECULE_DATE}}-$SCENARIO.txt"
        if [ $? != '0' ]; then
          .config/log error 'There were errors while running the test - results are available at `molecule/.results/{{.MOLECULE_DATE}}-$SCENARIO.txt`'
        else
          .config/log success 'Finished running the test - results are available at `molecule/.results/{{.MOLECULE_DATE}}-$SCENARIO.txt`'
        fi

  molecule:docker:prompt:
    interactive: true
    deps:
      - :install:modules:local
    cmds:
      - node .config/scripts/prompts/molecule/docker.js

  molecule:gcp:
    deps:
      - :install:python:requirements
      - :install:software:gcloud
    cmds:
      - task: molecule:gcp:preconditions
      - |
        .config/log info 'Running Google Cloud Platform Molecule test'
        {{.PYTHON_HANDLE}}molecule test -s gcp
        if [ $? != '0' ]; then
          .config/log error 'Finished running the Google Cloud Platform Molecule test (with errors)'
        else
          .config/log success 'Finished running Google Cloud Platform Molecule test'
        fi

  molecule:gcp:matrix:
    deps:
      - :install:modules:local
      - :install:software:gcloud
    vars:
      MOLECULE_DATE:
        sh: date '+%Y-%m-%d'
    cmds:
      - task: molecule:gcp:preconditions
      - mkdir -p molecule/.results
      - |
        .config/log info 'Piping results to `molecule/.results/{{.MOLECULE_DATE}}-gcp.txt` for compatibility chart data'
        PY_COLORS=0 {{.PYTHON_HANDLE}}molecule test -s gcp 2>&1 | tee "molecule/.results/{{.MOLECULE_DATE}}-gcp.txt"
        if [ $? != '0' ]; then
          .config/log error 'Errors encountered while running the test - results are available at `molecule/.results/{{.MOLECULE_DATE}}-gcp.txt`'
        else
          .config/log success 'Finished running the test - results are available at `molecule/.results/{{.MOLECULE_DATE}}-gcp.txt`'
        fi
      - |
        RESULTS="molecule/.results/{{.MOLECULE_DATE}}-gcp.txt"
        PLATFORM_LENGTH="$(yq e '.platforms | length' molecule/gcp/molecule.yml)"
        INDEX=0
        while [ $INDEX -lt $PLATFORM_LENGTH ]; do
          NAME="$(yq e '.platforms['$INDEX'].name' molecule/gcp/molecule.yml)"
          ALIAS="$(yq e '.platforms['$INDEX'].alias' molecule/gcp/molecule.yml)"
          sed -i -- 's/'"$NAME"'/'"$ALIAS"'/g' "$RESULTS"
          INDEX=$((INDEX+1))
        done

  molecule:gcp:preconditions:
    preconditions:
      - sh: '[ -n "$GCE_SERVICE_ACCOUNT_EMAIL" ]'
        msg: The GCE_SERVICE_ACCOUNT_EMAIL environment variable must be set (e.g. export
          GCE_SERVICE_ACCOUNT_EMAIL=molecule@megabyte-labs.iam.gserviceaccount.com).
      - sh: '[ -n "$GCE_CREDENTIALS_FILE" ]'
        msg: The GCE_CREDENTIALS_FILE environment variable must be set and pointing to the GCP
          service account JSON key (e.g. export GCE_CREDENTIALS_FILE=~/.config/gcp.json).
      - sh: test -f "$GCE_CREDENTIALS_FILE"
        msg: The GCE_CREDENTIALS_FILE environment variable is defined but is not pointing to a file that exists.
      - sh: '[ -n "$GCE_PROJECT_ID" ]'
        msg: The GCE_PROJECT_ID environment variable must be set (e.g. export GCE_PROJECT_ID=megabyte-labs)

  molecule:local:
    cmds:
      - node .config/scripts/prompts/molecule/local.js

  molecule:local:test:
    cmds:
      - |
        .config/log info 'Running Molecule test locally'
        {{.PYTHON_HANDLE}}molecule test -s local
        if [ $? != '0' ]; then
          .config/log error 'There was an error while running the Molecule test locally'
        else
          .config/log success 'The local Molecule test was successful!'
        fi

  molecule:ssh:cli:
    deps:
      - :install:python:requirements
    cmds:
      - |
        molecule test -s remote
        if [ $? != '0' ]; then
          .config/log error 'Errors encountered while running the remote Molecule test'
        else
          .config/log success 'Successfully ran the Molecule test on the SSH target'
        fi

  molecule:ssh:prompt:
    deps:
      - :install:modules:local
    cmds:
      - node .config/scripts/prompts/molecule/ssh.js

  molecule:virtualbox:
    deps:
      - :install:python:requirements
      - :install:software:sshpass
      - :install:software:vagrant
      - :install:software:virtualbox
    label: test:e2e
    desc: Runs a full E2E Molecule test for all supported operating systems
    summary: |
      # Run a full E2E Molecule test for all supported operating systems

      This task uses VirtualBox to run tests for all of our supported operating
      systems in parallel. It is very RAM intensive so, if you want to run this,
      your computer should have _at least 32GB of RAM_.

      **Run the full test:**
      `task ansible:test:molecule:virtualbox`

      **Generate the compatibility matrix used in the README.md:**
      `task ansible:test:molecule:virtualbox:matrix`
    env:
      # yamllint disable-line rule:truthy
      OBJC_DISABLE_INITIALIZE_FORK_SAFETY: YES
    cmds:
      - |
        .config/log info 'Running full VirtualBox Molecule E2E test'
        {{.PYTHON_HANDLE}}molecule test
        if [ $? != '0' ]; then
          .config/log error 'Errors were encountered while running the full VirtualBox Molecule E2E test'
        else
          .config/log success 'Finished running full VirtualBox Molecule E2E test'
        fi

  molecule:virtualbox:cli:
    deps:
      - :install:python:requirements
      - :install:software:sshpass
      - :install:software:vagrant
      - :install:software:virtualbox
    cmds:
      - |
        .config/log info 'Running VirtualBox Molecule test on platforms in the `{{.CLI_ARGS}}` group'
        MOLECULE_GROUP="{{.CLI_ARGS}}" {{.PYTHON_HANDLE}}molecule test
        if [ $? != '0' ]; then
          .config/log error 'Errors encountered while running the `{{.CLI_ARGS}}` VirtualBox Molecule test'
        else
          .config/log success 'Finished running `{{.CLI_ARGS}}` VirtualBox Molecule test'
        fi

  molecule:virtualbox:converge:
    deps:
      - :install:python:requirements
      - :install:software:sshpass
      - :install:software:vagrant
      - :install:software:virtualbox
    label: test:virtualbox
    desc: Provisions a desktop VirtualBox VM and then runs a Molecule test
    summary: |
      # Provision a desktop VirtualBox VM and then run a Molecule test

      This task opens a VM with an operating system of your choosing and then tests
      the project's play against it. It then leaves the VM open for inspection.

      **Example with interactive prompt for VM type:**
      `task test:molecule`

      **Example usage bypassing prompt:**
      `task test:molecule -- Archlinux`

      ## Available scenarios:

      * Archlinux
      * CentOS
      * Debian
      * Fedora
      * macOS
      * Ubuntu
      * Windows
    env:
      # yamllint disable-line rule:truthy
      OBJC_DISABLE_INITIALIZE_FORK_SAFETY: YES
    cmds:
      - task: molecule:virtualbox:converge:{{if .CLI_ARGS}}cli{{else}}prompt{{end}}

  molecule:virtualbox:converge:cli:
    cmds:
      - |
        .config/log info 'Running `{{.CLI_ARGS}}` VirtualBox Molecule converge play - this will leave the VirtualBox instance open for inspection'
        MOLECULE_GROUP={{.CLI_ARGS}} {{.PYTHON_HANDLE}}molecule converge
        if [ $? != '0' ]; then
          .config/log error 'Errors were encountered while running the `{{.CLI_ARGS}}` VirtualBox Molecule converge play'
        else
          .config/log success 'Finished running the `{{.CLI_ARGS}}` VirtualBox Molecule converge play - you are encouraged to inspect the VM'
        fi

  molecule:virtualbox:converge:prompt:
    interactive: true
    deps:
      - :install:modules:local
    cmds:
      - node .config/scripts/prompts/molecule/desktop.js

  molecule:virtualbox:matrix:
    deps:
      - :install:python:requirements
      - :install:software:sshpass
      - :install:software:vagrant
      - :install:software:virtualbox
    vars:
      MOLECULE_DATE:
        sh: date '+%Y-%m-%d'
    cmds:
      - mkdir -p molecule/.results
      - |
        .config/log info 'Running full E2E test with VirtualBox - results will be saved to `molecule/.results/{{.MOLECULE_DATE}}-default.txt`'
        PY_COLORS=0 {{.PYTHON_HANDLE}}molecule test | tee 'molecule/.results/{{.MOLECULE_DATE}}-default.txt'
        if [ $? != '0' ]; then
          .config/log error 'Errors were encountered while running the full E2E test - see `molecule/.results/{{.MOLECULE_DATE}}-default.txt` for details'
        else
          .config/log success 'Finished running full E2E test - `molecule/.results/{{.MOLECULE_DATE}}-default.txt` contains the results'
        fi

  molecule:virtualbox:prompt:
    deps:
      - :install:modules:local
    cmds:
      - node .config/scripts/prompts/molecule/virtualbox.js

  prompt:
    deps:
      - :install:modules:local
    cmds:
      - node .config/scripts/prompts/molecule/molecule.js

  vagrant:
    deps:
      - :install:python:requirements
      - :install:software:sshpass
      - :install:software:vagrant
      - :install:software:virtualbox
    label: test:vagrant
    desc: Runs the playbook using Vagrant
    summary: |
      # Run the playbook using Vagrant

      Using Vagrant, you can pick and choose which operating system and
      virtualization provider you want to use to test the playbook.

      ## Possible virtualization providers:

      * hyperv
      * libvirt
      * parallels
      * virtualbox
      * vmware_fusion
      * vmware_workstation

      ## Possible operating systems:

      * archlinux
      * centos
      * debian
      * fedora
      * macos
      * ubuntu
      * windows

      **Example opening interactive prompt:**
      `task test:vagrant`

      **Example bypassing interactive prompt:**
      `task test:vagrant -- --provider=vmware_workstation windows`
    cmds:
      - task: vagrant:{{if .CLI_ARGS}}cli{{else}}prompt{{end}}

  vagrant:cli:
    cmds:
      - |
        .config/log info 'Running `vagrant up {{.CLI_ARGS}}`'
        vagrant up {{.CLI_ARGS}}

  vagrant:prompt:
    deps:
      - :install:modules:local
    interactive: true
    cmds:
      - node .config/scripts/prompts/vagrant-up.js
