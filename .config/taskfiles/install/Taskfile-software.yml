# eslint-disable eslint-comments/disable-enable-pair, max-lines
---
version: '3'

tasks:
  act:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: act

  brew:
    deps:
      - common
    run: once
    cmds:
      - task: brew:{{OS}}

  brew:cask:
    deps:
      - brew
    run: when_changed
    cmds:
      - |
        .config/log info 'Installing the `{{.CASK}}` Homebrew cask'
        brew install --cask {{.CASK}}
        .config/log success 'Successfully installed the `{{.CASK}}` Homebrew cask'
    status:
      - type {{.CASK}} &> /dev/null || [[ "${container:=}" == "docker" ]]

  brew:darwin:
    cmds:
      - |
        .config/log info 'Ensuring Homebrew is installed and available'
        if [ -f "~/.local/homebrew/bin/brew" ] && ! type brew &> /dev/null; then
          .config/log info "Homebrew is already installed"
          eval "$(~/.local/homebrew/bin/brew shellenv)"
        elif ! type brew &> /dev/null; then
          rm -rf ~/.local/homebrew
          mkdir -p ~/.local/homebrew
          git clone https://github.com/Homebrew/brew ~/.local/homebrew
          eval "$(~/.local/homebrew/bin/brew shellenv)"
          .config/log info "Updating Homebrew"
          brew update --force --quiet
          chmod -R go-w "$(brew --prefix)/share/zsh"
          .config/log success "Successfully installed Homebrew"
        fi
      - task: brew:utils
    status:
      - type brew &> /dev/null || [[ "${container:=}" == "docker" ]]
      - type gcp > /dev/null
      - type gfind > /dev/null
      - type gsed > /dev/null
      - type ggrep > /dev/null
      - type gtar > /dev/null
      - type ggawk > /dev/null

  brew:formulae:
    deps:
      - brew
    run: when_changed
    cmds:
      - |
        .config/log info 'Installing the `{{.FORMULAE}}` Homebrew package'
        brew install {{.FORMULAE}}
        .config/log success 'Successfully installed the `{{.FORMULAE}}` Homebrew package'
    status:
      - type {{.FORMULAE}} &> /dev/null || [[ "${container:=}" == "docker" ]]

  brew:linux:
    run: once
    cmds:
      - |
        .config/log info 'Ensuring Homebrew is installed and available'
        function ensureSource() {
          if ! (grep "/bin/brew shellenv" < cat "$1" &> /dev/null); then
            echo "eval \"\$($(brew --prefix)/bin/brew shellenv)\"" >> "$1"
          fi
        }
        case "${SHELL}" in
          */bash*)
            if [[ -r "${HOME}/.bash_profile" ]]; then
              SHELL_PROFILE="${HOME}/.bash_profile"
            else
              SHELL_PROFILE="${HOME}/.profile"
            fi
            ;;
          */zsh*)
            SHELL_PROFILE="${HOME}/.zprofile"
            ;;
          *)
            SHELL_PROFILE="${HOME}/.profile"
            ;;
        esac
        if [ -f "~/.local/homebrew/bin/brew" ] && ! type brew &> /dev/null; then
          eval "$(~/.local/homebrew/bin/brew shellenv)"
          ensureSource "$SHELL_PROFILE"
        elif ! type brew &> /dev/null; then
          rm -rf ~/.local/homebrew
          mkdir -p ~/.local/homebrew
          git clone https://github.com/Homebrew/brew ~/.local/homebrew
          eval "$(~/.local/homebrew/bin/brew shellenv)"
          brew update --force --quiet
          ensureSource "$SHELL_PROFILE"
        fi
    status:
      - type brew &> /dev/null || [[ "${container:=}" == "docker" ]]

  brew:utils:
    cmds:
      - |
        .config/log info "Installing GNU-compatibility tools for macOS via Homebrew"
        brew install coreutils findutils gnu-indent gnu-sed gnutls grep gnu-tar gawk
        .config/log success "Successfully installed GNU-compatibility tools"

  brew:windows:
    cmds:
      - task: common:windows

  codeclimate:
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: codeclimate/formulae/codeclimate
    status:
      - type codeclimate > /dev/null

  common:
    run: once
    cmds:
      - task: common:{{OS}}

  common:darwin:
    cmds:
      - task: common:darwin:xcode

  common:darwin:xcode:
    cmds:
      - |
        .config/log info "Installing xcode-select"
        xcode-select --install
        .config/log success "Successfully installed xcode-select"
    status:
      - xcode-select -p 1> /dev/null

  common:linux:
    vars:
      LINUX_FAMILY:
        sh: |
          if [ -f "/etc/debian_version" ]; then
            echo "debian"
          elif [ -f "/etc/redhat-release" ]; then
            echo "redhat"
          elif [ -f "/etc/arch-release" ]; then
            echo "archlinux"
          else
            echo "unknown"
          fi
    cmds:
      - task: common:linux:{{.LINUX_FAMILY}}

  common:linux:archlinux:
    interactive: true
    cmds:
      - .config/log warn "Archlinux support for Homebrew is not very well documented.. if this does not work and you can get it working, please open a PR :)"
      - |
        sudo pacman update
        sudo pacman -S base-devel curl file git procps-ng
    status:
      - type curl &> /dev/null || [[ "${container:=}" == "docker" ]]
      - type git &> /dev/null || [[ "${container:=}" == "docker" ]]
      - ldconfig -p | grep base-devel || [[ "${container:=}" == "docker" ]]
      - ldconfig -p | grep file || [[ "${container:=}" == "docker" ]]
      - ldconfig -p | grep procps-ng || [[ "${container:=}" == "docker" ]]

  common:linux:debian:
    interactive: true
    cmds:
      - |
        .config/log info 'Attempting to install Homebrew dependencies (sudo password required)'
        sudo apt-get -y update
        sudo apt-get install -y build-essential curl file git procps
    status:
      - type curl &> /dev/null || [[ "${container:=}" == "docker" ]]
      - type git &> /dev/null || [[ "${container:=}" == "docker" ]]
      - dpkg-query -l build-essential &> /dev/null || [[ "${container:=}" == "docker" ]]
      - dpkg-query -l file &> /dev/null || [[ "${container:=}" == "docker" ]]
      - dpkg-query -l procps &> /dev/null || [[ "${container:=}" == "docker" ]]

  common:linux:redhat:
    interactive: true
    cmds:
      - mkdir -p "$HOME/.config/mblabs"
      - |
        if [ ! -f "$HOME/.config/mblabs/yum-devtools-check-ran" ]; then
          yum grouplist 'Development Tools' &> "$HOME/.config/mblabs/yum-devtools-check-ran"
          DEV_TOOLS_NOT_INSTALLED="$(grep 'No groups match' < "$HOME/.config/mblabs/yum-devtools-check-ran" > /dev/null)"
          if [ "$DEV_TOOLS_NOT_INSTALLED" == '0' ]; then
            sudo yum groupinstall -y 'Development Tools'
          fi
          touch "$HOME/.config/mblabs/yum-devtools-check-ran"
        fi
        if ! rpm --quiet --query curl file git procps-ng; then
          .config/log info 'Attempting to install Homebrew dependencies (sudo password required)'
          sudo yum install -y curl file git procps-ng
        fi
        if [ -f '/etc/os-release' ]; then
          source /etc/os-release
          if [ "$ID" == 'fedora' ] && [ "$VERSION_ID" -gt "29" ]; then
            if ! rpm --quiet --query libxcrypt-compat; then
              .config/log info 'Attempting to install Fedora-specific Homebrew dependency (sudo password required)'
              sudo yum -y install libxcrypt-compat
            fi
          fi
        fi

  common:linux:unknown:
    cmds:
      - .config/log warn 'You are using an operating system that we do not directly support. Please make sure
        the equivalent of `build-essential`, `curl`, `file`, `git`, and `procps` are installed.'

  common:windows:
    cmds:
      - .config/log error "Windows is not supported. Try using a Windows WSL environment."
      - exit 1

  container-structure-test:
    run: once
    cmds:
      - task: container-structure-test:{{OS}}

  container-structure-test:darwin:
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: container-structure-test

  container-structure-test:linux:
    cmds:
      - curl -LO https://storage.googleapis.com/container-structure-test/latest/container-structure-test-linux-amd64
      - chmod +x container-structure-test-linux-amd64
      - mkdir -p $HOME/.local/bin
      - export PATH=$PATH:$HOME/.local/bin
      - mv container-structure-test-linux-amd64 $HOME/.local/bin/container-structure-test
      - .config/log info 'Added container-structure-test to ~/.local/bin - ensure "$HOME/.local/bin" is in the PATH variable'
    status:
      - type container-structure-test > /dev/null

  container-structure-test:windows:
    cmds:
      - .config/log error "Windows is not supported. Try using a Windows WSL environment."
      - exit 1

  coreutils:
    deps:
      - brew
    run: once
    cmds:
      - |
        if [ '{{OS}}' == 'darwin' ] && ! type gsed > /dev/null; then
          .config/log info 'Installing GNU compatibility tools (i.e. `coreutils`)'
          brew install coreutils
          .config/log success 'Successfully installed GNU compatibility tools (i.e. `coreutils`)'
        fi
    status:
      - '[ "{{OS}}" == "darwin" ] || type gsed > /dev/null'

  docker:
    run: once
    cmds:
      - task: docker:{{OS}}

  docker:darwin:
    run: once
    cmds:
      - task: brew:cask
        vars:
          CASK: docker

  docker:linux:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: docker

  docker:windows:
    cmds:
      - task: common:windows

  dockle:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: goodwithtech/r/dockle
    status:
      - type dockle > /dev/null

  exiftool:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: exiftool

  gcloud:
    run: once
    cmds:
      - task: gcloud:{{OS}}
    status:
      - type gcloud > /dev/null

  gcloud:darwin:
    cmds:
      - task: brew:cask
        vars:
          CASK: google-cloud-sdk

  gcloud:linux:
    cmds:
      - task: :install:install-doctor
        vars:
          SOFTWARE: googlecloudsdk

  gcloud:windows:
    cmds:
      - task: common:windows

  gh:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: gh

  git:
    deps:
      - common
    run: once

  gitleaks:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: gitleaks

  glab:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: glab

  go:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: go

  grype:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: anchore/grype/grype
    status:
      - type grype > /dev/null

  jq:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: jq

  node:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: node

  pipx:
    run: once
    cmds:
      - task: pipx:{{OS}}
    status:
      - type pipx > /dev/null

  pipx:darwin:
    deps:
      - brew
    cmds:
      - .config/log info 'Installing pipx and running `pipx ensurepath`'
      - brew install pipx
      - pipx ensurepath
      - .config/log success 'Successfully installed pipx'

  pipx:linux:
    deps:
      - :install:software:python
    cmds:
      - .config/log info 'Installing pipx and running `pipx ensurepath`'
      - python3 -m pip install --user pipx
      - python3 -m pipx ensurepath
      - .config/log success 'Successfully installed pipx'

  pipx:windows:
    cmds:
      - .config/log error 'These scripts are not currently compatible with Windows. Try using WSL.'
      - exit 1

  poetry:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: poetry

  python:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: python

  rsync:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: rsync

  sshpass:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: hudochenkov/sshpass/sshpass
    status:
      - type sshpass > /dev/null

  tokei:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: tokei

  trivy:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: aquasecurity/trivy/trivy
    status:
      - type trivy > /dev/null

  vagrant:
    cmds:
      - task: vagrant:{{OS}}

  vagrant:darwin:
    run: once
    cmds:
      - task: brew:cask
        vars:
          CASK: vagrant

  vagrant:linux:
    run: once
    cmds:
      - task: :install:install-doctor
        vars:
          SOFTWARE: vagrant

  virtualbox:
    cmds:
      - task: virtualbox:{{OS}}

  virtualbox:darwin:
    run: once
    cmds:
      - task: brew:cask
        vars:
          CASK: virtualbox

  virtualbox:linux:
    run: once
    cmds:
      - task: :install:install-doctor
        vars:
          SOFTWARE: virtualbox

  vmware:
    cmds:
      - task: vmware:{{OS}}

  vmware:darwin:
    run: once
    cmds:
      - task: brew:cask
        vars:
          CASK: vmware-fusion

  vmware:linux:
    run: once
    cmds:
      - task: :install:install-doctor
        vars:
          SOFTWARE: vmware

  yq:
    run: once
    cmds:
      - task: brew:formulae
        vars:
          FORMULAE: yq
